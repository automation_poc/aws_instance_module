resource "aws_instance" "ec2_server" {
  count                  = "${var.ec2_count}"
  name                   = "${var.ec2_tag_app_name}0${count.index}"
  ami                    = "${var.ec2_ami_id}"
  instance_type          = "${var.ec2_instance_type}"
  key_name               = "${var.ec2_key_pair_name}"
  vpc_security_group_ids = ["${var.ec2_security_group_id}"]
  subnet_id              = "${var.ec2_subnet_id}"

  tags = {
    name = "${var.ec2_tag_app_name}0${count.index}"
    team = "${var.ec2_team_name}"
  }
}
