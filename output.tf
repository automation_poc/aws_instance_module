output "ec2_ip_addrs" {
  value = "${aws_instance.ec2_server.*.public_ip}"
}
