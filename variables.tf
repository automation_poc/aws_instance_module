# EC2 Variables

# Defined in application module
variable "ec2_count" {
  description = "Number of servers to provision"
}

variable "ec2_ami_id" {
  description = "Region-specific AMI to be used"
}

variable "ec2_instance_type" {
  description = "EC2 instance type"
  default     = "t2.micro"
}

variable "ec2_tag_app_name" {
  description = "App name for server name prefix"
}

# Defined in team env variables
variable "ec2_key_pair_name" {
  description = "EC2 key pair to be used for SSH connections"
}

variable "ec2_security_group_id" {
  description = "Security group to assign to EC2 instance"
}

variable "ec2_subnet_id" {
  description = "Subnet ID to assign to EC2 instance"
}

variable "ec2_team_name" {
  description = "Team name to asscoiate instance with"
}
